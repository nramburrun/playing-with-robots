function [robot, trajGenFunction, interceptFunction, controlFunction] = myFunction()
LT = 0;
p_ball_ev = [];
t_ball_ev = [];
prev_torque = zeros(1,8);
max_torque = zeros(1,8);
q_err = [];
q_plot_desired = [];
q_plot_actual = [];
time = [];

%% IN Report : Explain why its working
LT = 0;
links(1) =    Link('offset',0,'d',0.25,'a',0.25,'alpha',pi/2);
links(2) =    Link('offset',-pi/2,'d',0 , 'a',0, 'alpha', -pi/2);
links(3) =    Link('theta',0,'a',0 , 'alpha', pi/2);
links(4) =    Link('offset',pi/2,'d',0, 'a', 0, 'alpha', -pi/2); 
links(5) =    Link('offset',pi/2,'d',0,'a',0,'alpha',pi/2);
links(6) =    Link('theta',0,'a',0,'alpha',-pi/2);
links(7) =    Link('offset',-pi/2,'d',0,'a',0,'alpha',-pi/2);
links(8) =    Link('offset',0,'d',0,'a',0,'alpha',pi/2);

j_friction = 0.11;
m_inertia = 1.51e-8;
m_l1 = 0.5;
z = 0.25;
x = 0.25;
% Add the dynamic parameters to each of the links
B_friction = 0.11;
m_inertia = 1.51e-8;
m_l1 = 0.5;
z = 0.25;
x = 0.25;
% Add the dynamic parameters to each of the links
% Check the documentation of the Link class for details
% NOTE: Not all of these values are required/specified
% Must be set for a non-zero length link
Icm = (z^2)*m_l1 + (x^2)*m_l1;
d = sqrt((0.125^2) + (0.125^2));
I_p = Icm + m_l1*d^2;
rev_m = 0;
rev_inertia = [1 1 1];

links(1).m = m_l1;
links(1).r = [0.125 0 0.125]; % Must be a vector of 3 elements
links(1).I = [I_p 0 I_p]; % Can be 3 or 6 element vector, or a 3x3 matrix
links(1).B = B_friction;
links(1).G = 1;
links(1).Jm = 0;

links(2).m = 1; % Must be set for a non-zero length link
links(2).r = [0 0 0.5]; % Must be a vector of 3 elements
links(2).I = rev_inertia; % Can be 3 or 6 element vector, or a 3x3 matrix
links(2).B = B_friction;
links(2).G = 1;
links(2).Jm = m_inertia;

links(3).m = 1; % Must be set for a non-zero length link
links(3).r = [0 0 0.5]; % Must be a vector of 3 elements
links(3).I = [1/12 0 1/12]; % Can be 3 or 6 element vector, or a 3x3 matrix
links(3).B = B_friction;
links(3).G = 1;
links(3).Jm = m_inertia;

links(4).m = rev_m; % Must be set for a non-zero length link
links(4).r = [0 0 0]; % Must be a vector of 3 elements
links(4).I = rev_inertia; % Can be 3 or 6 element vector, or a 3x3 matrix
links(4).B = B_friction;
links(4).G = 1;
links(4).Jm = m_inertia;

links(5).m = rev_m; % Must be set for a non-zero length link
links(5).r = [0 0 0]; % Must be a vector of 3 elements
links(5).I = rev_inertia; % Can be 3 or 6 element vector, or a 3x3 matrix
links(5).B = B_friction;
links(5).G = 1;
links(5).Jm = m_inertia;

links(6).m = 1; % Must be set for a non-zero length link
links(6).r = [0 0 0.5]; % Must be a vector of 3 elements
links(6).I = [1/12 0 1/12]; % Can be 3 or 6 element vector, or a 3x3 matrix
links(6).B = B_friction;
links(6).G = 1;
links(6).Jm = m_inertia;

links(7).m = rev_m; % Must be set for a non-zero length link
links(7).r = [0 0 0]; % Must be a vector of 3 elements
links(7).I = rev_inertia; % Can be 3 or 6 element vector, or a 3x3 matrix
links(7).B = B_friction;
links(7).G = 1;
links(7).Jm = m_inertia;

tool_mass = 0.15;
links(8).m = tool_mass; % Must be set for a non-zero length link
links(8).r = [0 0 0]; % Must be a vector of 3 elements
links(8).I = [(tool_mass)*(0.15^2)/12 (tool_mass)*(0.15^2)/12 0]; % Can be 3 or 6 element vector, or a 3x3 matrix
links(8).B = B_friction;
links(8).G = 1;
links(8).Jm = m_inertia;
robot = SerialLink(links, 'name', 'YetAnotherBallGrabber');

%% LIMITS
R_Limits_min = -(3/4)*pi;
R_Limits_max = (3/4)*pi;

D_Limits_min = 0;
D_Limits_max = 1;


%% SET LIMITS
robot.qlim = [                          0 0;
                  R_Limits_min R_Limits_max; 
                  D_Limits_min D_Limits_max;  
                  R_Limits_min R_Limits_max;
                  R_Limits_min R_Limits_max;  
                  D_Limits_min D_Limits_max;
                  R_Limits_min R_Limits_max; 
                  R_Limits_min R_Limits_max];
% Assign constant offset for the tool frame (if needed)
theta_tool = pi/2;
d_tool = 0;
a_tool = LT;
alpha_tool = pi/2;

robot.tool = [ cos(theta_tool) -1*sin(theta_tool)*cos(alpha_tool) sin(theta_tool)*sin(alpha_tool) ...
    a_tool*cos(theta_tool); sin(theta_tool) cos(theta_tool)*cos(alpha_tool) -cos(theta_tool)*sin(alpha_tool) ...
    a_tool*sin(theta_tool); 0 sin(alpha_tool) cos(alpha_tool) d_tool; 0 0 0 1];

robot.plotopt = {'workspace' [-0.5,3.2,-1.5,1.5,-1.5,1.5]};

    %% Copy your intercept function here from Part 2 (b)
    % Write a ball intercept selection function, but do not change the name of the function
    function [p_goal, t_goal] = myInterceptFunction(p_ball, t_ball, m_ball)
        % Use the positions p_ball, times t_ball, and mass m_ball to calculate an intercept
        % The matrix p_ball is Px3 and contains P points where the ball has been located
        % The vector t_ball is Px1 and contains the P times associated with the rows of p_ball
        % The scalar m_ball is the mass of the ball
        
        % Note: At the beginning of the simulation, P=1. Write your function accordingly.
        
        % p_goal must be a 1x3 vector giving the intercept position at scalar time t_goal
        g = 9.81; % The constant acceleration due to gravity
        [rows, columns] = size(p_ball);
         p_goal = zeros(rows,3);
            if(rows == 1)
                t_goal = t_ball;
                p_goal = p_ball;
            else
                
            v_ball_diff = diff(p_ball) / 0.01;
            v_ball_avg = mean(v_ball_diff, 1);

            vel_x = v_ball_avg(1);
            vel_y = v_ball_avg(2);
            vel_z_initial = (p_ball(end) + 0.5*g*t_ball(end)^2) / t_ball(end);

            time_ws = abs((3 - 1.1) / vel_x);

            dist_x = 1.1;
            dist_y = vel_y*time_ws;
            dist_z = (vel_z_initial*time_ws - 0.5*g*time_ws^2);
            

            %%Edge case. IF it goes above and hits the workspace
            if( dist_z > 0.7 )
                time_ws = max(roots([-0.5*g vel_z_initial -0.7]));
                dist_x = 3.0 + vel_x*time_ws;
                dist_y = vel_y * time_ws;
                dist_z = vel_z_initial*time_ws - 0.5*g*time_ws^2;
            end

            p_goal = [dist_x dist_y dist_z];
            t_goal = time_ws;  
            
            end
        % You will need to replace this with your calculated intercept time
    end
    interceptFunction = @myInterceptFunction; % Do not modify this line or your tests will fail

    
    
    %% Copy your moving target trajectory generation function here from Part 2 (b)
    % Write a trajectory generation function, but do not change the name of the function
    function [q_traj, qd_traj, qdd_traj] = myTrajGenFunction(p_goal, t_goal, t_now, q_now, qd_now)
        %% Generates a trajectory from the current state to the goal position and time
        % Your trajectory must satisfy every joint's position and velocity limits
        % The end effector must reach the goal position p_goal in at most t_goal seconds
        % If the end effector reaches the goal position before t_goal, qd_traj must end with zero velocity
        % The end effector must be within the allowable orientations at the end of the trajectory 
        
        % q_traj and qd_traj must be MxN matrices, with one row per 0.01s time step and one column per joint
        % The first element of each of these matrices must be the current state q and qd
        q_goal = myIKFunction(p_goal);
        qd_final = [0 0 0 0 0 0 0 0];
        %% Plotting for Report
%         q_plot_desired(end+1,:) = q_goal;
%         q_plot_actual(end+1,:) = q_now;
%         time(end+1) = t_now + 0.01;
%         plot(q_plot_desired(:,1),time,'ob',q_plot_actual(:,1),time,'or');
%         axis([0 1 -Inf Inf]);
%         title('Rev. Joint q1');
%         legend('Desired position','Actual Position');
%         xlabel('Time');
%         ylabel('Joint Position (rads)')
%         % q_traj and qd_traj must be MxN matrices, with one row per 0.01s time step and one column per joint
        %% End Plot
        dt = 28;
        err = (q_goal - q_now);
        [Q,QD,QDD] = jtraj(q_now,q_now + 1.0*err,dt,qd_now ,qd_final); 

        q_traj = Q;% You wilt_giakl need to replace this with your joint position trajectory
        qd_traj = QD;  % You will need to replace this with your joint velocity trajector
        qdd_traj = QDD;
  
        %% You may insert a copy of your inverse kinematics function here from Part 1 (e) if desired
        % If you are not reusing your IK function, ignore or erase the myIKFunction definition below
        function q = myIKFunction(T)
            % Generate a set of joint positions q that will achieve the desired transformation T
            % Remember that T is a desired position and orientation of the end effector frame
            dx = round(T(1),5);
            dy = round(T(2),5);
            dz = round(T(3),5);   
            dx = dx - 0.25;
            dz = dz - 0.25;
            
            d2 = 1;
            x1 = sqrt(1 - (dz^2));
            if (dy == 0)
                if(dx <= x1)
                    %%use d2 only in this case
                    d2 = sqrt(dx^2 + dz^2);
                    q1 = -(pi/2 - atan2(dz,dx));
                    x2 = 0;
                    q3 = -q1;
                    d5 = 0;
                    q4 = 0;
                    q7 = 0;
                    q6 = 0;
                else
                    %cannot reach with only one prismatic
                    x2 = dx - x1;
                    d2 = 1;
                    q1 = -(pi/2 -atan2(dz,x1));
                    d5 = sqrt(x2^2 + dy^2);
                    q4 = atan2(dz,x2);       
                    q3 = -q1;
                    q7 = 0;
                    q6 = pi/2;      
                end
            
                % if q1 > limit
                else
                %if we have a y component, split the use
                    x1 = 1*dx;
                    d2 = sqrt(dz^2 + (x1)^2);
                    q1 = -(pi/2 -atan2(dz,x1));
                    x2 = 0.0*dx;
                    d5 = sqrt(dy^2 + x2^2);
                    q4 = atan2(dy,x2);
                    q3 = -q1;
                    q7 = 0;
                    q6 = -q4;
            end
            q1 = q1 + pi/2;
            q3 = -q1;
            q = [0 q1 d2 q3 q4 d5 q6 q7];
        end
    end
    trajGenFunction = @myTrajGenFunction; % Do not modify this line or your tests will fai
    
    
    % Write a torque controller function, but do not change the name of the function
function torque = myControlFunction(~, t_now, q_now, qd_now, p_ball_last)
        %% Generates a torque command given the current state of the robot and ball
        % t_now is the current time of the simulation
        % q_now is the current position of the robot's joints
        % qd_now is the current velocity of the robot's joints
        % p_ball_last is the last seen position of the ball (simulating a sensor reading)
        
        %% Using t_now and p_ball_last, update the p_ball and t_ball variables
       p_ball_ev(end+1,:) = p_ball_last;
       t_ball_ev(end+1,:) = t_now;
       m_ball = 0.05;
      
       %% Call your interrupt function to generate an updated intercept position and time
       [p_goal, t_goal] = myInterceptFunction(p_ball_ev, t_ball_ev, m_ball);
       [q_traj, qd_traj, qdd_traj] = myTrajGenFunction(p_goal, t_goal, t_now, q_now, qd_now);
        %% Write a controller which attempts to follow your generated trajectory
        mid = (t_goal)/2;
        if(t_now <= mid)
           Kd= 40;
           Kp = 1;
          qd_traj = qd_traj.*Kp;
          qdd_traj = qdd_traj.*Kd;
        else
            Kd = 0.01;
            Kp = 1;
            qdd_traj = qdd_traj.*Kd;
            qd_traj = qd_traj.*Kp;
        end

        %% Call your trajectory generation function to generate an updated trajector
        % Depending on your controller, you may also return a desired acceleration qdd_traj
        
        %replace q_now with the difference with joint positions and qd_now with difference
        % in joint velocities
       % q_desired = q_traj(end,:);
       % q_err = abs(q_now - q_desired);
        
      %q_traj(end,:)
        torque = robot.rne(q_traj(2,:),qd_traj(2,:), qdd_traj(2,:));
        
        %% Set Max. torque recorded - 4c
%         for indx = 1:8
%             max_torque(indx) = torque(indx);
%             if(max_torque(indx) < torque(indx))
%                 max_torque(indx) = torque(indx);
%             end
%         end
%         
%         %%Set Threshold for max torque
%         for i = 1:8
%             if(torque(i) > max_torque(i))
%                 torque(i) = 0.5*max_torque(i);
%             end
%         end
        
        %% Check limits. If above, then apply a negative torque to the joints to counter balance it
        if(q_traj(2,2) < robot.qlim(2,1) || q_traj(2,2) > robot.qlim(2,2))
            torque(2) = -prev_torque(2)*2.5;
        end
        
        if(q_traj(2,3) < robot.qlim(3,1) || q_traj(2,3) > robot.qlim(3,2))
            torque(3) = -prev_torque(3)*2.5;
        end
        
        if(q_traj(2,4) < robot.qlim(4,1) || q_traj(2,4) > robot.qlim(4,2))
            torque(4) = -prev_torque(4)*2.5;
        end
        
        if(q_traj(2,5) < robot.qlim(5,1) || q_traj(2,5) > robot.qlim(5,2))
            torque(5) = -prev_torque(5)*2.5;
        end
        
        if(q_traj(2,6) < robot.qlim(6,1) || q_traj(2,6) > robot.qlim(6,2))
            torque(6) = -prev_torque(6)*2.5;
        end
        
        if(q_traj(2,7) < robot.qlim(7,1) || q_traj(2,7) > robot.qlim(7,2))
            torque(7) = -prev_torque(7)*2.5;
        end
        
        if(q_traj(2,8) < robot.qlim(8,1) || q_traj(2,8) > robot.qlim(8,2))
            torque(8) = -prev_torque(8)*2.5;
        end
        prev_torque = torque;
        
%         fk_data(1:3,4) = robot.fkine(q_now)
%         
%         stop_cond = abs(fk_data - p_goal);
%         cart = sqrt(stop_cond(1)^2 + stop_cond(2)^2 + stop_cond(3)^2);
%         if(cart <= 0.001)
%            torque = -0; 
%         end
    end
    controlFunction = @myControlFunction; % Do not modify this line or your tests will fail
end